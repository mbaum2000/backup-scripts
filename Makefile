DESTDIR := 

BINDIR = $(DESTDIR)/usr/bin
CONFDIR = $(DESTDIR)/etc

UDEV_RULES = $(DESTDIR)/lib/udev/rules.d
CRON_DAILY = $(CONFDIR)/cron.daily
CRON_MONTHLY = $(CONFDIR)/cron.monthly

RSYNC_SYSTEM_BACKUP_CONF = $(CONFDIR)/rsync-system-backup

clean:

all: make

make:

install: install-rsync-system-backup install-rsync-profile install-update-vcs install-backup-calendars install-backup-minecraft

install-rsync-system-backup:
	install -d $(BINDIR)
	install -d $(CRON_DAILY)
	install -d $(UDEV_RULES)
	install -d $(RSYNC_SYSTEM_BACKUP_CONF)
	install -T -m 755 scripts/rsync-system-backup.sh $(BINDIR)/rsync-system-backup
	install -T -m 644 udev/hdd-backup.rules $(UDEV_RULES)/99-hdd-backup.rules
	install -T -m 644 conf/backuprc $(RSYNC_SYSTEM_BACKUP_CONF)/backuprc
	install -T -m 644 conf/email-recipients $(RSYNC_SYSTEM_BACKUP_CONF)/email-recipients
	install -T -m 644 conf/exclude-backup $(RSYNC_SYSTEM_BACKUP_CONF)/exclude-backup
	install -T -m 644 conf/mailrc $(RSYNC_SYSTEM_BACKUP_CONF)/mailrc
	ln -s /usr/bin/rsync-system-backup $(CRON_DAILY)

install-rsync-profile:
	install -d $(BINDIR)
	install -T -m 755 scripts/rsync-profile.sh $(BINDIR)/rsync-profile

install-update-vcs:
	install -d $(BINDIR)
	install -T -m 755 scripts/update-vcs.sh $(BINDIR)/update-vcs

install-backup-calendars:
	install -d $(BINDIR)
	install -d $(CONFDIR)
	install -d $(CRON_MONTHLY)
	install -T -m 755 scripts/backup-calendars.sh $(BINDIR)/backup-calendars
	install -T -m 644 conf/calrc $(CONFDIR)/calrc
	ln -s /usr/bin/backup-calendars $(CRON_MONTHLY)

install-backup-minecraft:
	install -d $(BINDIR)
	install -T -m 755 scripts/backup-minecraft.sh $(BINDIR)/backup-minecraft

