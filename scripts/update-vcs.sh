#!/bin/bash

HASH='\\#'

if [[ $EUID -eq 0 ]]; then
    VCS_DIRS_FILE="/etc/vcsrc"
else
    VCS_DIRS_FILE="${HOME}/.vcsrc"
fi

colorize() {
    ("${@}" | while read line; do
        echo -e "\t\033[34m\033[1m${line}\033[0m"
    done) 2>&1| while read line; do
        echo -e "\t\033[31m\033[1m${line}\033[0m"
    done
}

check_line() {
    TYPE="$1"
    SOURCE="$2"
    DEST="$3"
  
    if [ -d "${DEST}" ]; then
        case "${TYPE}" in
          "")
            ;;
          git)
            echo "Updating ${TYPE} in: ${DEST}"
            pushd "${DEST}" > /dev/null
            colorize git fetch
            colorize git pull #origin master:master
            popd > /dev/null
            ;;
          svn)
            echo "Updating ${TYPE} in: ${DEST}"
            pushd "${DEST}" > /dev/null
            colorize svn up
            popd > /dev/null
            ;;
          *)
            echo "Unknown VCS type: ${TYPE}"
            ;;
        esac
    else
        case "${TYPE}" in
          "")
            ;;
          git)
            echo "Fetching ${TYPE} repo ${SOURCE} in: ${DEST}"
            colorize git clone "${SOURCE}" "${DEST}"
            ;;
          svn)
            echo "Fetching ${TYPE} repo ${SOURCE} in: ${DEST}"
            colorize svn checkout "${SOURCE}" "${DEST}"
            ;;
          *)
            echo "Unknown VCS type: ${TYPE}"
            ;;
        esac
    fi
}

case "$1" in
  --help)
    echo "Usage:"
    echo "$0 [vcsrc file]"
    exit
    ;;
  "")
    ;;
  *)
    VCS_DIRS_FILE="$1"
    ;;
esac

if [ ! -e "${VCS_DIRS_FILE}" ]; then
    echo "Error: ${VCS_DIRS_FILE} is missing."
    exit 1
fi

echo "Using ${VCS_DIRS_FILE}"
while read line; do
    eval check_line "${line}"
done < ${VCS_DIRS_FILE}

