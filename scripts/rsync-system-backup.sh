#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "Error: This utility requires root."
    exit 1
fi

DEST_DEVICE="$1"
SEND_EMAIL_ON_START="$2"

MATCH_LABEL="backup"

RSYNC_ROOT="/"
RSYNC_CONFIG="/etc/rsync-system-backup"
RSYNC_SCRIPT="/usr/bin/rsync-system-backup"

EMAIL_RECIPIENTS="$(cat ${RSYNC_CONFIG}/email-recipients)"
EMAIL_SENDER="${USER}@$(hostname)"
RSYNC_EXTRA_ARGS=

. ${RSYNC_CONFIG}/backuprc

export MAILRC="${RSYNC_CONFIG}/mailrc"

fork_process() {
    export NOFORK=1
    /usr/bin/setsid ${RSYNC_SCRIPT} $@ > /dev/null 2>&1 < /dev/null &
}

if [ -z "${DEST_DEVICE}" ]; then
    __found=0
    while read uuid; do
        if [ "$(e2label "/dev/disk/by-uuid/${uuid}")" == "${MATCH_LABEL}" ]; then
            ((__found++))
            fork_process "${uuid}"
        fi
    done < <(ls -1 /dev/disk/by-uuid/)
    if [ "${__found}" -eq 0 ]; then
        echo -e "[WARNING] No Backup Media Detected!" | mail -s "[WARNING] Backup for $(hostname) did not occur!" -r "${EMAIL_SENDER}" ${EMAIL_RECIPIENTS}
    fi
    exit
elif [ -z "${NOFORK}" ]; then
    fork_process $@
    exit
fi

DEST_DEVICE_PATH="/dev/disk/by-uuid/${DEST_DEVICE}"
RSYNC_DEST_MOUNT="/backups/${DEST_DEVICE}"
RSYNC_DEST_ROOT="${RSYNC_DEST_MOUNT}/$(hostname)"
RSYNC_FILES="${RSYNC_CONFIG}/files-backup"
RSYNC_INCLUDES="${RSYNC_CONFIG}/include-backup"
RSYNC_EXCLUDES="${RSYNC_CONFIG}/exclude-backup"

DATE=$(date "+%Y-%m-%dT%H_%M_%S")
LOGFILE="/tmp/backup-${DATE}.log"
DEVICE_INFO="/tmp/${DEST_DEVICE}.info"
BACKUP_TITLE="Backup of $(hostname) on $(date)"

EXCLUDES_OPT=
if [ -e ${RSYNC_EXCLUDES} ]; then
    EXCLUDES_OPT="--exclude-from=${RSYNC_EXCLUDES}"
fi

INCLUDES_OPT=
if [ -e ${RSYNC_INCLUDES} ]; then
    INCLUDES_OPT="--include-from=${RSYNC_INCLUDES}"
fi

FILES_OPT=
if [ -e ${RSYNC_FILES} ]; then
    FILES_OPT="--files-from=${RSYNC_FILES}"
fi

send_email() {
    echo -e "$1\nLog file attached.\n\n$(cat ${DEVICE_INFO})" | mail -s "$1" -a ${LOGFILE} -r "${EMAIL_SENDER}" ${EMAIL_RECIPIENTS}
}

refresh_device_info() {
    echo "Disk Usage for HDD $(cat "${RSYNC_DEST_MOUNT}/ID"):" > ${DEVICE_INFO}
    df -h | grep -e Filesystem -e "${RSYNC_DEST_MOUNT}" >> ${DEVICE_INFO}
}

check_mount() {
    if mount | grep ${RSYNC_DEST_MOUNT}; then
        return 0
    else
        return 1
    fi
}

mount_fs() {
    echo "Mounting Filesystem: ${RSYNC_DEST_MOUNT}"
    if check_mount; then
        echo "Filesystem already mounted, assuming backup currently in progress"
        return 1
    fi
    mkdir -p ${RSYNC_DEST_MOUNT}
    if ! mount ${DEST_DEVICE_PATH} ${RSYNC_DEST_MOUNT}; then
        echo "Failure Mounting Filesystem"
        return 1
    fi
    refresh_device_info
}

umount_fs() {
    refresh_device_info
    echo "UnMounting Filesystem: ${RSYNC_DEST_MOUNT}"
    if ! umount ${RSYNC_DEST_MOUNT}; then
        echo "Failure Mounting Filesystem"
    fi
    rmdir ${RSYNC_DEST_MOUNT}
}

rsync_backup() {
    if ! mount_fs; then
        return 1
    fi

    mkdir -p ${RSYNC_DEST_ROOT}
    LAST_BACKUP=$(ls -1r ${RSYNC_DEST_ROOT} | grep '^backup-[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}T[0-9]\{2\}_[0-9]\{2\}_[0-9]\{2\}$' -m1)
    LAST_PARTIAL=$(ls -1r ${RSYNC_DEST_ROOT} | grep '^partial-[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}T[0-9]\{2\}_[0-9]\{2\}_[0-9]\{2\}$' -m1)

    LINK_DEST_OPT=
    if [ "${LAST_BACKUP}" != "" ]; then
        LINK_DEST_OPT="${LINK_DEST_OPT} --link-dest=${RSYNC_DEST_ROOT}/${LAST_BACKUP}"
    fi
    if [ "${LAST_PARTIAL}" != "" ]; then
        LINK_DEST_OPT="${LINK_DEST_OPT} --link-dest=${RSYNC_DEST_ROOT}/${LAST_PARTIAL}"
    fi

    mkdir ${RSYNC_DEST_ROOT}/partial-${DATE}

    echo "Executing: rsync --archive --recursive ${RSYNC_EXTRA_ARGS} ${EXCLUDES_OPT} ${INCLUDES_OPT} ${FILES_OPT} ${LINK_DEST_OPT} ${RSYNC_ROOT} ${RSYNC_DEST_ROOT}/partial-${DATE}"

    if rsync --archive --recursive \
        ${RSYNC_EXTRA_ARGS} \
        ${EXCLUDES_OPT} \
        ${INCLUDES_OPT} \
        ${FILES_OPT} \
        ${LINK_DEST_OPT} \
        ${RSYNC_ROOT} ${RSYNC_DEST_ROOT}/partial-${DATE} 2>&1
    then
        touch ${RSYNC_DEST_ROOT}/partial-${DATE}
        mv ${RSYNC_DEST_ROOT}/partial-${DATE} ${RSYNC_DEST_ROOT}/backup-${DATE}
        umount_fs
        echo "Backup Completed: $(date)"
        return 0;
    else
        touch ${RSYNC_DEST_ROOT}/partial-${DATE}
        umount_fs
        echo "Backup Failed: $(date)"
        return 1;
    fi
}

echo "Backup at ${RSYNC_DEST_ROOT}/backup-${DATE}" >> ${LOGFILE}
echo "Logfile at ${LOGFILE}" >> ${LOGFILE}
echo "----------------------------------------------" >> ${LOGFILE}
echo "" >> ${LOGFILE}
echo "Backup Started: $(date)" >> ${LOGFILE}

sleep 10
if [ ! -e "${DEST_DEVICE_PATH}" ]; then
    echo "Device unplugged, Backup Cancelled: $(date)" >> ${LOGFILE}
    send_email "[CANCELLED] ${BACKUP_TITLE}"
    exit
fi

if [ ! -z "${SEND_EMAIL_ON_START}" ]; then
    send_email "[STARTED] ${BACKUP_TITLE}"
fi

if rsync_backup >> ${LOGFILE}; then
    if [ "${SEND_EMAIL_SUCCESS_ON_DAY}" == "$(date +%u)" -o -z "${SEND_EMAIL_SUCCESS_ON_DAY}" ]; then
        send_email "[COMPLETED] ${BACKUP_TITLE}"
    fi
else
    send_email "[FAILED] ${BACKUP_TITLE}"
fi

