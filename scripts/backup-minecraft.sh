#!/bin/sh

TIMESTAMP="$(date +%Y-%m-%d_%H:%M:%S/)"
DATA_DIR="${HOME}/.minecraft/"
BACK_DIR="${HOME}/.minecraft-backups/"
if [ ! -d "${BACK_DIR}" ]; then
  mkdir "${BACK_DIR}"
fi

PREV_BACK="${BACK_DIR}$(ls -1t ${BACK_DIR} | head -1)"
LINK_DEST=""

if [ -d "${PREV_BACK}" ]; then
  LINK_DEST="--link-dest=${PREV_BACK}"
fi

rsync -auv --progress ${LINK_DEST} ${DATA_DIR} ${BACK_DIR}${TIMESTAMP}
