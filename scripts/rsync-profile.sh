#!/bin/bash

if [ "${UID}" -eq "0" ]; then
  RSYNC_ROOT="/"
  RSYNC_CONFIG="/etc/rsync/"
else
  RSYNC_ROOT="${HOME}"
  RSYNC_CONFIG="${HOME}/.rsync/"
fi

RSYNC_SERVER=
RSYNC_PATH=
RSYNC_USER=${USER}
RSYNC_FILES="${RSYNC_CONFIG}/rsync.files-profile"
RSYNC_INCLUDES="${RSYNC_CONFIG}/rsync.include-profile"
RSYNC_EXCLUDES="${RSYNC_CONFIG}/rsync.exclude-profile"

if [ -e "$1" ]; then
  . $1
elif [ -e ${RSYNC_CONFIG}/rsync.profile ]; then
  . ${RSYNC_CONFIG}/rsync.profile
fi

if [ "${RSYNC_SERVER}" = "" ]; then
  echo "Error, RSYNC_SERVER not set!" >&2
  exit 1
fi
if [ "${RSYNC_PATH}" = "" ]; then
  echo "Error, RSYNC_PATH not set!" >&2
  exit 1
fi

time (
  echo " -- Pushing local files --"
  rsync --archive --recursive --update -v --include-from=${RSYNC_INCLUDES} --exclude-from=${RSYNC_EXCLUDES} --files-from=${RSYNC_FILES} ${RSYNC_ROOT} ${RSYNC_USER}@${RSYNC_SERVER}:${RSYNC_PATH}/${USER}
  echo " -- Pulling remote files --"
  rsync --archive --recursive --update -v -K --include-from=${RSYNC_INCLUDES} --exclude-from=${RSYNC_EXCLUDES} --files-from=${RSYNC_FILES} ${RSYNC_USER}@${RSYNC_SERVER}:${RSYNC_PATH}/${USER} ${RSYNC_ROOT}
)
