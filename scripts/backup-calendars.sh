#!/bin/bash

TIMESTAMP="$(date +%Y-%m-%d_%H:%M:%S/)"

if [[ $EUID -eq 0 ]]; then
    BACKUP_DIR="/var/lib/gcal-backup/${TIMESTAMP}"
    CALRC_FILE="/etc/calrc"
else
    BACKUP_DIR="${HOME}/.gcal-backup/${TIMESTAMP}"
    CALRC_FILE="${HOME}/.calrc"
fi

colorize_wget() {
    ("${@}" | while read line; do
        echo -e "\t\033[1;34m$(echo ${line} | sed 's/ -> /\n\t\t-> /')\033[0m"
    done) 2>&1| while read line; do
        echo -e "\t\033[1;31m$(echo ${line} | sed 's/ -> /\n\t\t-> /')\033[0m"
    done
}

check_line() {
    NAME="$1"
    ACCOUNT="$(echo $2 | sed 's/@/%40/g')"
    ID="$3"

    if [ "${NAME}" = "" -o "${ACCOUNT}" = "" -o "${ID}" = "" ]; then
        return
    fi

    mkdir -p ${BACKUP_DIR}
    echo -e "\033[32mBacking up Calendar: \033[1;32m${NAME}\033[0m"
    #echo -e "\t\033[1;34mXML:\033[0m"
    #colorize_wget wget -nv https://www.google.com/calendar/feeds/${ACCOUNT}/private-${ID}/basic -O ${BACKUP_DIR}/${NAME}.xml
    echo -e "\t\033[1;34miCal:\033[0m"
    colorize_wget wget -nv https://www.google.com/calendar/ical/${ACCOUNT}/private-${ID}/basic.ics -O ${BACKUP_DIR}/${NAME}.ics
    echo ""
}

case "$1" in
  --help)
    echo "Usage:"
    echo "$0 [calrc file]"
    exit
    ;;
  "")
    ;;
  *)
    CALRC_FILE="$1"
    ;;
esac

if [ ! -e "${CALRC_FILE}" ]; then
    echo "Error: ${CALRC_FILE} is missing."
    exit 1
fi

echo "Using ${CALRC_FILE}"
while read line; do
    eval check_line "${line}"
done < ${CALRC_FILE}

